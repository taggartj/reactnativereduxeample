import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './reducers/index';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from './screens/Main';
import Dashboard from './screens/Dashboard';

const store = createStore(rootReducer);

const Stack = createStackNavigator();


export default class App extends React.Component {
  render() {
     return (
       <Provider store={store}>
         <NavigationContainer>
            <Stack.Navigator>
              <Stack.Screen name="Main" component={Main} />
              <Stack.Screen name="Dashboard" component={Dashboard} />
            </Stack.Navigator>
         </NavigationContainer>
       </Provider>
     );
  }
}


