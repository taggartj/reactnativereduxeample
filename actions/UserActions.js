
export function setUserEmail(email) {
    let action = {type: 'SET_EMAIL', payload: email}; 
    return action;
}

export function setUserPassword(password) {
    let action = {type: 'SET_PASSWORD', payload: password}; 
    return action;
}

export function loginUser(email, password) {
    /// here do the lodgic for log in aka 
    // REST POST call
    let session = {
        token: '123'
    }
    let action = {type: 'SET_USER_LOGGED_IN', payload: session}; 
    return action;
}

export function logOutUser() {
    /// here do the lodgic for logout aka 
    // REST POST call
    let action = {type: 'USER_LOG_OUT'}; 
    return action;
}