import React from 'react';
import { 
    StyleSheet,
    Text,
    View,
    Button,
    Alert,
    TextInput,
    Linking} from 'react-native';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCoffee, faBars } from '@fortawesome/free-solid-svg-icons';
import { Header } from 'react-native-elements';


const style = StyleSheet.create({
    icon: {
      color: 'blue',
    },
    topBar: {
        backgroundColor: '#FDD7E4',
        alignSelf: 'stretch',
      height: 65,
      backgroundColor: '#fff',
    },
    content: {
        alignItems: 'center', 
        justifyContent: 'center' 
    }
  })

class Dashboard extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          title: "Dashboard"
      }
    }

    componentDidMount() {
        if (this.props.user.UserReducer.loggedIn === false) {
            // kick them.
            this.props.navigation.navigate('Main');
        }
    }

    handleMenuClick = () => {
        Alert.alert('Do stuff');
    }

    render() {
        return(
            
             <View style={style.content}>
                 <Header
                  leftComponent={{ icon: 'menu', color: '#fff', onPress: this.handleMenuClick }}
                  centerComponent={{ text: this.state.title , style: { color: '#fff' } }}
                  rightComponent={{ icon: 'home', color: '#fff' }}
                />

                <Text>This Example  Uses: </Text>
                <Button title="React-native-elements" onPress={ ()=>{ Linking.openURL('https://react-native-elements.github.io/react-native-elements/')}} />
                <Text>And </Text>
                <FontAwesomeIcon style={ style.icon }  size={ 30 } icon={ faCoffee } onPress={ ()=>{ Linking.openURL('https://fontawesome.com/')}} />
                
                
           </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state,
    }
}
  
export default connect(mapStateToProps)(Dashboard);

