import React from 'react';
import { 
    StyleSheet,
    Text,
    View,
    Button,
    Alert,
    TextInput} from 'react-native';
import { connect } from 'react-redux';

import NetInfo from "@react-native-community/netinfo";

import {
    setUserEmail,
    setUserPassword,
    loginUser,
    logOutUser
} from "../actions/UserActions";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textStyle : {
       fontSize: 30,
       color: '#000'
    },
    inputText: { 
        height: 40,
        width: 200,
        borderColor: 'gray',
        borderWidth: 1,
        fontSize: 20,
    },
  });


class Main extends React.Component {
    constructor(props) {
      super(props);
      ///console.log(props.navigation);
      this.state = {
        user: props.user.UserReducer,
        email_text: '',
        password: '',
        online: false,
      }
    }

    componentDidMount= () => {
        console.log('hello');

        NetInfo.fetch().then(state => {
           // console.log("Connection type", state.type);
           // console.log("Is connected?", state.isConnected);
           this.setState({online: state.isConnected})
          });
    }


    // Validate here.
    clickBtn = () => {
        let email = this.state.email_text;
        let password = this.state.password;
        if (!email) {
            return Alert.alert('Email is required');
        }
        if (!password) {
            return Alert.alert('Password is required');
        }
        // @todo validate email.
        this.props.dispatch(setUserEmail(email));
        this.props.dispatch(setUserPassword(email));
        setTimeout(() => {
            this.login();
        }, 1000);
        
    }

    login = () => {
        var user = this.props.user.UserReducer;
        if (user.email && user.password) {
            // log in here.
            this.props.dispatch(loginUser(user.email, user.password));
        }
        else {
            return Alert.alert('Please provide all details');
        }
    }

    logoutUserClick = () => {
        this.setState({
            password: '',
            email_text: '',
            
        });
        this.props.dispatch(logOutUser());
    }




    onChangeEmailText = (text) =>  {
        this.setState({
            email_text: text
        });
    }

    onChangePasswordText = (text) =>  {
        this.setState({
            password: text
        });
    }

    

    render() {
        if (this.state.online === false) {
            return (
                <View style={styles.container}>
                    <Text>Sorry you must have an active Internet connection</Text> 
                </View>
            );
        }
        else {



        let email = this.props.user.UserReducer.email;
        let IsLoggedin = this.props.user.UserReducer.loggedIn;

        if (IsLoggedin === false) {
            return (
                <View
                   style={styles.container}>
                   <Text style={styles.textStyle}>Please Log In:</Text>
                   <Text style={styles.textStyle}>{email}</Text>
        
                   <Text style={styles.textStyle}>Username: </Text>
                   <TextInput  style={styles.inputText}
                     placeholder="Enter your email"
                     onChangeText={this.onChangeEmailText}
                     value={this.state.email_text}
                   />
                   <Text style={styles.textStyle}>Password:</Text>
                   <TextInput  style={styles.inputText}
                     placeholder="Enter your Password"
                     onChangeText={this.onChangePasswordText}
                     value={this.state.password}
                     secureTextEntry={true}
                   />
        
                   <Button
                     title="Log In"
                     onPress={this.clickBtn }
                    />
                 </View>
               );
        }
        else {
            // Just redirect.
            this.props.navigation.navigate('Dashboard');
            return (

                <View
                   style={styles.container}>
                   <Text style={styles.textStyle}>Hello {email}</Text>

                   <Button
                     title="Go To Dashboard"
                     onPress={() => this.props.navigation.navigate('Dashboard')}
                   />
                
                   <Button
                     title="Log out"
                     onPress={this.logoutUserClick }
                    />
                </View>
            ); 

        }
      }
    }
  }

function mapStateToProps(state) {
    return {
        user: state,
    }
}
  
export default connect(mapStateToProps)(Main);