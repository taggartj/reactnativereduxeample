const user = {
    email: '',
    password: '',
    loggedIn: false,
    session: {},
};

const userReducer = (state = user, action) => {
  switch (action.type) {
    case 'SET_EMAIL':
      state = {...state, email: action.payload}
      break;

    case 'SET_PASSWORD':
      state = {...state, password: action.payload}
      break;

    case 'SET_USER_LOGGED_IN':
      state = {...state, loggedIn: true, session: action.payload}
      break;
      
    case 'USER_LOG_OUT':
      state = {
        email: '',
        password: '',
        loggedIn: false,
        session: {},
      };
      break;
      
    
    default:
      state = state
      break;
  }
  return state;
};

export default userReducer;
